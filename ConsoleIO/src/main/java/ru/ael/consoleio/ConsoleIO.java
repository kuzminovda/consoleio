/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package ru.ael.consoleio;

import java.util.Scanner;

/**
 *
 * @author student
 */
public class ConsoleIO {

    private static Scanner consoleScanner;
    //Создание экземпляра класса Scanner
    private static String EXIT = "exit";
    private static String PERSON = "Персона";
    private static String STUDENT = "Студент";
    private static String PROFESSOR = "Преподаватель";
    private static String GATES = "Гейтс";
    

    public static void main(String[] args) {

        /*
        System.out.println("Создаю экземпляр класса Person...");
        Person p1 = new Person();
        System.out.println("Создан экземпляр, вывожу на экран...");
        System.out.println(p1);
        
        System.out.println("Создаю экземпляр класса Student...");
        Student s0 = new Student();
        System.out.println("Создан экземпляр, вывожу на экран...");
        System.out.println(s0);
        
        System.out.println("Создаю экземпляр класса Professor...");
        Professor pr1 = new Professor();
        System.out.println("Создан экземпляр, вывожу на экран...");
        System.out.println(pr1);
         */
        System.out.println("Консольный клиент TCP");
        consoleScanner = new Scanner(System.in);
        //Класс Scanner является потоком ввода и связывает
        //текущую программу с клавиатурой

        while (consoleScanner.hasNextLine()) {
            String line = consoleScanner.nextLine();
            System.out.println(">" + line);
            if (line.trim().equalsIgnoreCase(EXIT)) {
                System.out.println("Выход из программы");
                break;
                //ЗАДАНИЕ: дописать обработку команд отправки экземляра студента
                //после формирования экземляра студента
            }
            if (line.trim().equalsIgnoreCase(STUDENT)) {
                 createStudent();
            }
                 
            if (line.trim().equalsIgnoreCase(PERSON)){
                createPerson();
            }
            
            if (line.trim().equalsIgnoreCase(PROFESSOR)){
                createProfessor();
            }
             
        }

    }
    

    public static Person createPerson(){
        System.out.println("Создаю экземпляр класса Person...");
        Person p1 = new Person();
        Scanner PersonScanner = new Scanner(System.in);
        String line = PersonScanner.nextLine();
        System.out.println("Введите фамилию персоны");
        p1.fam = line;
        System.out.println("");
        return p1;
        
    }
    
    public static Student createStudent(){
        Student s1 = new Student();
        
        return s1;
    }
    
    public static Professor createProfessor(){
        Professor pr1 = new Professor();
        
        return pr1;
    }
}

/*public static Student createStudent() {
        Scanner StudentScanner = new Scanner(System.in); //создание потока данных с клавиатуры для ввода полей класса Студент
        Student s1 = new Student(); //создаем пустой экземляр класса Студент с именем s1
        
        System.out.println("Создание экземпляра класса Student...");
        System.out.print("Введите фамилию студента:");
        String line = StudentScanner.nextLine();
       //s1.setFam("Гейтс");
      
        System.out.println("Введите имя студента");
        //s1.setIm("Данила");
        System.out.println("Введите отчество студента");
        //s1.setOtch("Андреевич");
        System.out.println("Введите курс студента");
        //s1.setKurs("1 курс");
        System.out.println("Введите группу студента");
        //s1.setGrup("ПИ(б)-21");
        
        //s1.setFam(null);
        
      
        return s1;
        //StudentScanner = new Scanner

    }
 */
    

//}


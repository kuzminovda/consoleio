/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.consoleio;

/**
 *
 * @author student
 */
public class Person {
    
    public String id;

    public String fam;

    public String im;

    public String otch;

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", fam=" + fam + ", im=" + im + ", otch=" + otch + '}';
    }
    
    
}

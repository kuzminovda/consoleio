/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.consoleio;

/**
 *
 * @author student
 */
public class Student extends Person {

    private String kurs;
    
    private String grup;

    @Override
    public String toString() {
        return "Student{"+ "id=" + id + ", fam=" + fam + ", im=" + im + ", otch=" + otch + ", kurs=" + kurs + ", grup=" + grup + '}';
    }

    
}
